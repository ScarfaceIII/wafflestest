data_mysql_root_password=password
data_mysql_version=5.6

declare -ag data_mysql_packages

data_mysql_packages=(mysql-server-${data_mysql_version} mysql-client-${data_mysql_version} mysql-utilities )
