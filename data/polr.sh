declare -ag data_polr_packages

data_polr_packages=( git php5-mysqlnd php5-mcrypt )

data_polr_url=https://github.com/cydrobolt/polr
data_polr_tag=2.0.0b1

data_polr_path=/var/www/html/polr/
