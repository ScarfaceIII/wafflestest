stdlib.title "apache/php"

for package in ${data_apache_php_packages[@]}
do
	stdlib.apt --package ${package}
done

if [ ! -f /etc/apache2/mods-enabled/php5.load ]; then
	stdlib.capture_error a2enmod php5
	stdlib_state_change=true
fi

if [ ! -f /etc/apache2/mods-enabled/rewrite.load ]; then
	stdlib.capture_error a2enmod rewrite
	stdlib_state_change=true
fi

if [[ $stdlib_state_change == true ]]; then
	stdlib.capture_error /etc/init.d/apache2 restart
fi
