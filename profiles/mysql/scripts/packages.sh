stdlib.title "mysql/packages"

for package in ${data_mysql_packages[@]}
do
	stdlib.apt --package ${package}
done

