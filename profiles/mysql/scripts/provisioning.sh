stdlib.title "mysql/provisioning"

hostname=$(hostname | sed -e 's/_/\\\_/g')

mysql.user --user root --host localhost --password ${data_mysql_root_password}
mysql.mycnf --filename "/root/.my.cnf" --user root --password ${data_mysql_root_password}

mysql.user --state absent --user root --host 127.0.0.1 --password ""
mysql.user --state absent --user root --host ::1 --password ""
mysql.user --state absent --user "" --host localhost --password ""
mysql.user --state absent --user root --host $hostname --password ""
mysql.user --state absent --user "" --host $hostname --password ""

mysql.database --state absent --name test

stdlib.ini --file /etc/mysql/my.cnf --section mysqld --option bind-address --value 0.0.0.0

mysql.database --state present --name ${data_mysql_database_name}
mysql.user --state present --user ${data_mysql_username} --password ${data_mysql_password} --host localhost
mysql.grant --user ${data_mysql_username} --host localhost --database ${data_mysql_database_name} --privileges ${data_mysql_database_grants}

if [[ $stdlib_state_change == true ]]; then
  stdlib.capture_error /etc/init.d/mysql restart
fi

