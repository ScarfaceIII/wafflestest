stdlib.title "polr/install"

for package in ${data_polr_packages[@]}
do
	stdlib.apt --package ${package}
done

stdlib.git --name ${data_polr_path} --source ${data_polr_url} --tag ${data_polr_tag}

stdlib.file --name ${data_polr_path}. --mode 0775 --group www-data
stdlib.file --name ${data_polr_path}.htaccess --mode 0775 --group www-data

# Can't do a recursive chown with stdlib.file 
/bin/chown -R www-data:www-data ${data_polr_path}

stdlib.file --name /etc/apache2/sites-enabled/001-polr.conf --mode 0755 --source "${profile_files}/001-polr.conf"

apache.setting --path "VirtualHost=*:80" \
                 --key ServerName --value ${data_polr_public_dns} \
                                --file /etc/apache2/sites-enabled/001-polr.conf

apache.setting --path "VirtualHost=*:80" \
                 --key ServerAlias --value ${data_polr_public_dns} \
                                --file /etc/apache2/sites-enabled/001-polr.conf

stdlib.capture_error /etc/init.d/apache2 restart
