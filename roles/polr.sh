# https://github.com/Cydrobolt/polr/blob/master/README.md
stdlib.data mysql-common
stdlib.data mysql-polr
stdlib.data apache-common
stdlib.data apache-php
stdlib.data polr
stdlib.enable_augeas

stdlib.enable_mysql
stdlib.profile mysql/packages
stdlib.profile mysql/provisioning

stdlib.enable_apache
stdlib.profile apache/packages
stdlib.profile apache/php

stdlib.profile polr/install
